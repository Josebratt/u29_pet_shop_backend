const express = require("express");
const Producto = require("../models/productos");
const router = express.Router();

/**
 * Lista de productos
 */
router.get("/", (req, res) => {
  Producto.find()
    .then((data) => {
      if (data.length) {
        return res.status(200).send(data);
      } else {
        return res.status(404).json({
          success: false,
          message: "no existen productos en la base de datos",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

/**
 * Obtener productos por ID
 */
router.get("/:id", (req, res) => {
  Producto.findById(req.params.id)
    .then((data) => {
      if (data) {
        return res.status(200).send(data);
      } else {
        return res.status(404).json({
          success: false,
          message: "El producto no existe en la base de datos",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

/**
 * Guardar productos
 */
router.post("/", (req, res) => {
  let producto = new Producto({
    nombre: req.body.nombre,
    codigo: req.body.codigo,
    unidadMedida: req.body.unidadMedida,
    valorCompra: req.body.valorCompra,
    valorVenta: req.body.valorVenta,
    cantidad: req.body.cantidad,
    categoria: req.body.categoria,
  });

  producto
    .save()
    .then(() => {
      res.status(201).json({
        success: true,
        message: "Producto creado exitosamente",
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
});

/**
 * Actualizar productos
 */
router.put("/:id", (req, res) => {
  Producto.findByIdAndUpdate(
    req.params.id,
    {
      nombre: req.body.nombre,
      codigo: req.body.codigo,
      unidadMedida: req.body.unidadMedida,
      valorCompra: req.body.valorCompra,
      valorVenta: req.body.valorVenta,
      cantidad: req.body.cantidad,
      categoria: req.body.categoria,
    },
    { new: true }
  )
    .then(() => {
      res.status(201).json({
        success: true,
        message: "Producto actualizado exitosamente",
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

/**
 * Eliminar productos
 */
 router.delete("/:id", (req, res) => {
    Producto.findByIdAndDelete(req.params.id)
      .then((data) => {
        if (data) {
          return res.status(200).json({
            success: true,
            message: "Producto eliminado exitosamente",
          });
        } else {
          return res.status(404).json({
            success: false,
            message: "Producto no encontrado",
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          success: false,
          error: err,
        });
      });
  });

  module.exports = router;