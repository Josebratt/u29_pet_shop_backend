/**
 * se importan las librerias a usar
 */
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require("dotenv/config");
/**
 * se crea una constante que contiene todas las funciones de express
 */
const app = express();
const authJwt = require('./jwt/jsonwt')

/**
 * se habilita cors para recibir peticiones de otro servidor
 */
app.use(cors());
app.options("*", cors);

/**
 * middleware
 */
app.use(express.json());
app.use(authJwt());

/**
 * permite request desde cualquier origen
 */
 app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/**
 * definimos la ruta al controlador
 */
const rutaProv = require("./controllers/proveedor");
const rutaProd = require("./controllers/producto");
const rutaUser = require("./controllers/usuario");

/**
 * definimos las url a la que nos vamos a conectar
 */
app.use("/proveedor", rutaProv);
app.use("/producto", rutaProd);
app.use("/usuario", rutaUser);

/**
 * definimos la conexion a la base de datos
 */
mongoose
  .connect(
    "mongodb+srv://Josebratt:ktxKQumvGPcnIip9@cluster0.elyz6.mongodb.net/petshop?retryWrites=true&w=majority"
  )
  .then(() => {
    console.log("Conectado a mongoDB");
  })
  .catch((err) => console.log(err));

/**
 * definimos el puerto a usar
 */
const port = process.env.PORT || 5000;

/**
 * definimos el inicio del servidor en el puerto asignado
 */
app.listen(port, () => {
  console.log(`El servidor esta corriendo en http://localhost:${port}`);
});
